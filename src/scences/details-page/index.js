import React, { Component } from 'react'
import { View, Text, FlatList, Image } from 'react-native'

class Details extends Component {
  constructor(props) {
    super(props)
    this.numCol = 3
  }

  formatData (data, numColumns) {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
      data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
      numberOfElementsLastRow++;
    }
  
    return data;
  }

  render() {
    const { 
      imageWrapperStyle, 
      imageStyle, 
      gameNameStyle, 
      wrapperStyle,
      descriptionTextStyle,
      priceTextStyle,
      priceValueStyle
    } = styles

    return (
      <View style={wrapperStyle}>
        <Text style={gameNameStyle}>{this.props.game.name}</Text>
        <Text style={descriptionTextStyle}>Description:</Text>
        <Text>{this.props.game.overlay.top_blurb_md}</Text>
        <Text style={priceTextStyle}>Price:</Text>
        <Text style={priceValueStyle}>{this.props.game.price_to_pay}$</Text>
        <View style={{ marginTop: 20 }}>
          <FlatList 
            data={this.formatData(this.props.game.overlay.images, this.numCol)}
            renderItem={({item, index}) => {
              return (
                <View style={imageWrapperStyle}>
                  <Image style={imageStyle}
                    source={{uri: `https://playosmo.com/${item.image_url}`}} />
                </View>
              )
            }}
            numColumns={this.numCol}
            keyExtractor={item => Math.random()}
          />
        </View>
      </View>
    )
  }
}

const styles = {
  wrapperStyle: { 
    backgroundColor: 'white', borderRadius: 10, margin: 15, padding: 10 
  },
  gameNameStyle: {
    color: '#804A9A', fontSize: 25, fontWeight: '700', marginBottom: 20
  },
  imageWrapperStyle: {
    flex:1, height: 120, margin: 7.5
  },
  imageStyle: {
    flex: 1, alignSelf: 'stretch', width: undefined, height: 150
  },
  descriptionTextStyle: {
    color: '#000', fontSize: 18, fontWeight: '500'
  },
  priceTextStyle: { 
    color: '#000', fontSize: 18, fontWeight: '500', marginTop: 20  
  },
  priceValueStyle: {
    color: 'red', fontSize: 18, fontWeight: '700'
  }
}

export default Details