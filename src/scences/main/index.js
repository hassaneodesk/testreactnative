import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import { Actions } from 'react-native-router-flux'
class Main extends Component {
  renderGamesList() {
    const { gameContainerStyle, gameNameTextStyle, imageStyle } = styles

    return this.props.gamesList.map((game, key) => {
      return (
        <TouchableOpacity style={gameContainerStyle} key={key} onPress={() => Actions.details({ game })}>
          <Text style={gameNameTextStyle}>{game.name}</Text>
          <Image style={imageStyle}
            source={{uri: `https://playosmo.com/${game.overlay.images[0].image_url}`}} 
          />
          <Text>{game.overlay.one_liner}</Text>
        </TouchableOpacity>
      )
    })
  }

  render() {
    const { container } = styles
    
    return (
      <ScrollView style={container}>
        <View style={{ flex: 1 }}>
          {this.renderGamesList()}
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: '#efefef',
    paddingLeft: 15,
    paddingRight: 15
  },
  gameContainerStyle: {
    marginBottom: 15,
    borderRadius: 10,
    backgroundColor: 'white',
    padding: 10,
    height: 400,
  },
  gameNameTextStyle: {
    color: '#804A9A',
    fontWeight: '700',
    fontSize: 20
  },
  imageStyle: {
    flex: 1,
    alignSelf: 'stretch',
    width: undefined,
    height: undefined
  }
});

export default Main