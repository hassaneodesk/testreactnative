import React from 'react';
import Main from './src/scences/main'
import Details from './src/scences/details-page'
import { View } from 'react-native'
import { Scene, Router, Stack } from 'react-native-router-flux'


export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      games: []
    }
  }

  componentWillMount() {
    fetch('https://playosmo.com/api/prices/productprices?country=US')
      .then(resp => resp.json())
      .then(data => {
        let categories = []
        let games = []

        Object.entries(data.games).map(([key, value]) => categories.push(value))
        categories.forEach(category => Object.entries(category).map(([key, value]) => games.push(value)))
        this.setState({ games })
      })
      .catch(() => alert('error'))
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Router>
          <Stack key="root" renderBackButton={()=>(null)} headerBackTitle={false} >
            <Scene key="main" headerBackTitle={false} title="EXPLORE GAMES" initial gamesList={this.state.games} component={Main} />
            <Scene key="details" component={Details} headerBackTitle={false} title="GAME DETAILS" />
          </Stack>
        </Router>
      </View>
    )
  }
}
